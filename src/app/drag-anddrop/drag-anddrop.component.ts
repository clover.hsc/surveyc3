import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-drag-anddrop',
  templateUrl: './drag-anddrop.component.html',
  styleUrls: ['./drag-anddrop.component.scss'],
})
export class DragAnddropComponent implements OnInit {
  private el: ElementRef;
  private rightIndex: HTMLElement;
  private startElement: HTMLElement;

  public location = { x: 0, y: 0 };
  constructor(private element: ElementRef) {
    this.el = this.element;
  }

  ngOnInit() {
    // this.dragenter();
  }

  /**
   * when candidate enter drop area.
   * @param ev drag event
   */
  enter_area(ev: any) {
    const classValueList: string = ev.target.classList.value;
    console.log(classValueList);
    // console.log(`-- enter area ${ev.target.id} --`);
    if (classValueList.indexOf('source') !== -1) {
      console.log('none');
      ev.dataTransfer.effectAllowed = 'none';
    } else {
      console.log('move');
      ev.dataTransfer.effectAllowed = 'move';
    }
  }

  dragstart_handler(ev: any) {
    console.log('dragStart');
    ev.dataTransfer.effectAllowed = 'move';
    ev.dataTransfer.setData('text/plain', ev.target.id);
    this.startElement = ev.target.parentNode;
  }

  dragover_handler(ev) {
    ev.preventDefault();
  }

  async drop_handler(ev) {
    ev.preventDefault();
    const classValueList: string = ev.target.classList.value;
    console.log(classValueList);
    console.log(`-- enter area ${ev.target.id} --`);
    const span$ = ev.target.querySelector('span');
    if (span$) {
      span$.classList.add('hide');
    }
    if (classValueList.indexOf('source') !== -1) {
      console.log('none');
      ev.dataTransfer.effectAllowed = 'none';
    } else {
      console.log('drop');
      ev.dataTransfer.effectAllowed = 'move';
      const data = ev.dataTransfer.getData('text/plain');
      console.log(data);
      if (data) {
        ev.target.appendChild(this.el.nativeElement.querySelector(`#${data}`));
        ev.target.classList.remove('loc');

        // show right candidate index number.
        if (this.rightIndex) {
          this.rightIndex.classList.toggle('hide');
        }
      }
    }
  }

  drag_enter(event: any) {
    console.log(event.target.className);
    if (event.target.className === 'block') {
      event.target.classList.add('loc');
    }
  }

  drag_leave(event: any) {
    console.log(event);
    event.target.classList.remove('loc');
  }

  drag_start(event: any) {
    event.target.classList.remove('loc');
    this.rightIndex = event.target.previousElementSibling;
  }

  showSomething() {}

  goBack() {}
}
