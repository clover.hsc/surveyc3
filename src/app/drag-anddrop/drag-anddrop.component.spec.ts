import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragAnddropComponent } from './drag-anddrop.component';

describe('DragAnddropComponent', () => {
  let component: DragAnddropComponent;
  let fixture: ComponentFixture<DragAnddropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragAnddropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragAnddropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
