import { Component, OnInit } from '@angular/core';
import { of, from } from 'rxjs';
import { concatMap, delay, mergeMap, map, concatAll, mergeAll } from 'rxjs/operators';

@Component({
  selector: 'app-concatsomething',
  templateUrl: './concatsomething.component.html',
  styleUrls: ['./concatsomething.component.scss']
})
export class ConcatsomethingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.test();
  }

  test() {
    const getData = (param) => {
      return of(`Return: ${param}`).pipe(delay(Math.random() * 1000));
    };

    // from([1, 2, 3, 4, 5]).pipe(
    //   // map with concatAll()
    //   map(param => getData(param)),
    //   concatAll()
    // ).subscribe((val) => console.log(val));

    // from([1, 2, 3, 4, 5]).pipe(
    //   // concatMap
    //   concatMap((param) => getData(param))
    // ).subscribe((val) => console.log(val));

    // from([1, 2, 3, 4, 5]).pipe(
    //   // map with mergeAll()
    //   map(param => getData(param)),
    //   mergeAll()
    // ).subscribe((val) => console.log(val));

    from([1, 2, 3, 4, 5]).pipe(
      // mergeMap()
      mergeMap((param) => getData(param))
    ).subscribe((val) => console.log(val));
  }

  /**
   * ConcatMap v.s MergeMap.
   */
  concMvsMergM() {
    const source = of(2000, 1000);

    // when finish previous one then subscribe next one.
    const example = source.pipe(
      concatMap(val => of(`Delayed by: ${val}ms`).pipe(delay(val)))
    );

    // output: with concatMap: Delayed by: 2000ms, with concatMap: Delayed by: 1000ms
    const subscribe = example.subscribe(val => console.log(`With concatMap: ${val}`));

    // show the different between concatMap and mergeMap
    const mergeMapExample = source.pipe(
      delay(5000),
      mergeMap(val => of(`Delayed by: ${val}ms`).pipe(delay(val)))
    ).subscribe(val => console.log(`With mergeMap: ${val}`));
  }

}
