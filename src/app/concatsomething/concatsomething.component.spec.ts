import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcatsomethingComponent } from './concatsomething.component';

describe('ConcatsomethingComponent', () => {
  let component: ConcatsomethingComponent;
  let fixture: ComponentFixture<ConcatsomethingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcatsomethingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcatsomethingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
