import { PrimeComponent } from './prime/prime.component';
import { DragAnddropComponent } from './drag-anddrop/drag-anddrop.component';
import { ChartComponent } from './chart/chart.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConcatsomethingComponent } from './concatsomething/concatsomething.component';

const routes: Routes = [
  { path: 'drag', component: DragAnddropComponent },
  { path: 'prime', component: PrimeComponent },
  { path: 'concat', component: ConcatsomethingComponent },
  { path: '', component: ChartComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
