import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartComponent } from './chart/chart.component';
import { DragAnddropComponent } from './drag-anddrop/drag-anddrop.component';
import { PrimeComponent } from './prime/prime.component';
import { TreeModule } from 'primeng/tree';
import { ConcatsomethingComponent } from './concatsomething/concatsomething.component';

@NgModule({
  declarations: [AppComponent, ChartComponent, DragAnddropComponent, PrimeComponent, ConcatsomethingComponent],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule, BrowserAnimationsModule, TreeModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
