import { Component, OnInit, ElementRef, ViewEncapsulation } from '@angular/core';
import * as visavail from 'visavail';
import * as c3 from 'c3';

@Component({
  selector: 'app-chart',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit {
  private el: ElementRef;

  constructor(private element: ElementRef) {
    this.el = this.element;
  }

  ngOnInit() {
    this.lineChart();
    this.splineChart2();
    this.stackBar();
    // this.stackBarJson();
    this.DrawVisavail();
  }

  splineChart2(): void {
    c3.generate({
      bindto: '#line-chart2',
      data: {
        x: 'time_stamp',
        xFormat: '%Y-%m-%d %H:%M:%S',
        xLocaltime: false,
        columns: [
          [
            'time_stamp',
            '2019-11-26 04:10:02',
            '2019-11-26 04:20:32',
            '2019-11-26 04:30:02',
            '2019-11-26 04:40:02',
            '2019-11-26 04:50:02',
            '2019-11-26 04:59:02',
          ],
          ['Memory_Bandwidth', 30, 20, 10, 40, 15, 25],
          ['IO_Bandwidth', 50, 20, 10, 40, 15, 25],
          ['Process_Bandwidth', 30, 33, 10, 80, 20, 25],
          ['Memory_throttled', 10, 24, 15, 29, 41, 55],
        ],
        type: 'spline',
        types: {
          Memory_Bandwidth: 'area-spline',
          IO_Bandwidth: 'area-spline',
          Process_Bandwidth: 'area-spline',
          Memory_throttled: 'area-spline',
        },
        onclick: (res) => {
          console.log(res);
        },
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: (x) => {
              return x.toLocaleString();
            },
          },
        },
      },
    });
  }

  /**
   * Draw a status bar by visavail js
   */
  DrawVisavail() {
    const dataset = [
      {
        measure: 'Annual Report',
        measure_url: 'http://www.github.com/flrs/visavail',
        interval_s: 60 * 60,
        data: [
          ['2019-11-25 02:00:00', 0],
          ['2019-11-25 05:22:51', 1],
          ['2019-11-25 06:14:12', 1],
          ['2019-11-25 10:20:05', 1],
        ],
      },
    ];

    const options = {
      id_div_container: 'visavail_container',
      id_div_graph: 'visavail_graph',
      onClickBlock: (res: any, i: any) => {
        console.log(res);
        console.log(i);
      },
      date_in_utc: false,
      show_y_title: true,
    };

    const chart = visavail.generate(options, dataset);
  }

  /**
   * Try data of json on stack bar chart.
   * rotated axis.
   */
  stackBarJson() {
    c3.generate({
      bindto: '#json-stack-bar2',
      axis: {
        rotated: true,
        x: {
          show: false,
          tick: {
            centered: true,
          },
        },
        y: {
          show: true,
        },
      },
      data: {
        type: 'bar',
        json: [
          { name: '15-1', usage: 200, total: 200 },
          { name: '15-2', usage: 100, total: 300 },
          { name: '15-3', usage: 300, total: 200 },
          { name: '15-4', usage: 400, total: 100 },
        ],
        keys: {
          // x: 'name', // it's possible to specify 'x' when category axis
          value: ['usage', 'total'],
        },
        groups: [['usage', 'total']],
        labels: true,
        onclick: (d, ele) => {
          console.log(d);
          console.log(ele);
        },
      },
      grid: {
        y: {
          show: false,
        },
        x: {
          show: false,
        },
      },
    });
  }

  /**
   * A simple stack bar chart.
   */
  stackBar(): void {
    c3.generate({
      bindto: '#stack-bar1',
      axis: {
        rotated: false,
        x: {
          type: 'category',
          show: true,
          tick: {
            centered: true,
            count: 10,
          },
        },
        y: {
          show: true,
          max: 5,
        },
      },
      data: {
        type: 'bar',
        x: 'x',
        columns: [
          // ['data1', -30, 200, 200, 400, -150, 250],
          // ['data2', 130, 100, -100, 200, -150, 50],
          // ['data3', -230, 200, 200, -300, 250, 250],
          ['x', '2019-11-14 09:41:03', '2019-11-14 09:46:03', '2019-11-14 09:51:03'],
          ['15-1', 1, 1, 1],
        ],
        labels: true,
        // groups: [['data1', 'data2', 'data3']],

        onclick: (d, ele) => {
          console.log(d);
          console.log(ele);
        },
      },
      grid: {
        y: {
          show: false,
        },
        x: {
          show: false,
        },
      },
    });

    this.el.nativeElement.querySelectorAll('.c3-bar').forEach((item: any) => item.classList.add('mybar-stroke'));
  }

  /**
   * A area spline chart.
   */
  lineChart(): void {
    c3.generate({
      bindto: '#line-chart1',
      data: {
        x: 'time_stamp',
        xFormat: '%Y-%m-%dT%H:%M:%SZ',
        xLocaltime: false,
        columns: [
          [
            'time_stamp',
            '2019-11-26T04:10:02Z',
            '2019-11-26T04:20:32Z',
            '2019-11-26T04:30:02Z',
            '2019-11-26T04:40:02Z',
            '2019-11-26T04:50:02Z',
            '2019-11-26T04:59:02Z',
          ],
          ['Memory_Bandwidth', 30, 200, 100, 400, 150, 250],
          ['IO_Bandwidth', 50, 20, 10, 40, 15, 25],
          ['Process_Bandwidth', 30, 33, 10, 800, 200, 250],
          ['Memory_throttled', 10, 24, 15, 29, 41, 55],
        ],
        type: 'spline',
        types: {
          Memory_Bandwidth: 'area-spline',
          IO_Bandwidth: 'area-spline',
          Process_Bandwidth: 'area-spline',
          Memory_throttled: 'area-spline',
        },
        onclick: (res) => {
          console.log(res);
        },
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: (x) => {
              return x.toLocaleString();
            },
          },
        },
      },
    });
  }
}
