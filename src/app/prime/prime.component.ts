import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { TreeNode } from 'primeng/api';


@Component({
  selector: 'app-prime',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './prime.component.html',
  styleUrls: ['./prime.component.scss']
})
export class PrimeComponent implements OnInit {

  files: TreeNode[];
  files1: TreeNode[];
  files2: TreeNode[];

  selectedFile: TreeNode;
  constructor() { }

  ngOnInit() {

    // lazy load child
    this.getLazyChild().subscribe((tree) => {
      this.files = tree.data;
    });

    // Basic Tree initail
    this.renderTree().subscribe((tree) => this.files1 = tree.data);

    // Select Tree initial
    this.renderTree().subscribe((tree) => this.files2 = tree.data);
  }

  /**
   * Try select tree event.
   * @param event : $event
   */
  nodeSelect(event) {
    console.log(event.node);
    console.log(this.selectedFile.data);
  }

  loadNode(event) {
    console.log(event);
    if (event.node) {
      this.getLazyChild2().subscribe(nodes => event.node.children = nodes.data);
    }
  }

  getLazyChild2() {
    const child = {
      data:
        [
          {
            label: 'Clover',
            data: 'Node 0',
            expandedIcon: 'fa fa-folder-open',
            collapsedIcon: 'fa fa-folder',
            leaf: false
          },
          {
            label: 'Hsc',
            data: 'Node 1',
            expandedIcon: 'fa fa-folder-open',
            collapsedIcon: 'fa fa-folder',
            leaf: false
          },
          {
            label: 'David',
            data: 'Node 2',
            expandedIcon: 'fa fa-folder-open',
            collapsedIcon: 'fa fa-folder',
            leaf: false
          }
        ]
    };

    return of(child).pipe(delay(1000));
  }

  getLazyChild() {
    const child = {
      data:
        [
          {
            label: 'Lazy Node 0',
            data: 'Node 0',
            expandedIcon: 'fa fa-folder-open',
            collapsedIcon: 'fa fa-folder',
            leaf: false
          },
          {
            label: 'Lazy Node 1',
            data: 'Node 1',
            expandedIcon: 'fa fa-folder-open',
            collapsedIcon: 'fa fa-folder',
            leaf: false
          },
          {
            label: 'Lazy Node 1',
            data: 'Node 2',
            expandedIcon: 'fa fa-folder-open',
            collapsedIcon: 'fa fa-folder',
            leaf: false
          }
        ]
    };

    return of(child).pipe(delay(1000));
  }

  renderTree() {
    const structure = {
      data:
        [
          {
            label: 'Documents',
            data: 'Rack',
            expandedIcon: 'expandIcon',
            collapsedIcon: 'expandIcon',
            children: [{
              label: 'Work',
              data: 'Work Folder',
              expandedIcon: '',
              collapsedIcon: '',
              children: [
                {
                  label: 'Expenses.doc', icon: '', data: {
                    Manufacturer: 'Supermicro',
                    DrawerHeight: 2,
                    DrawerLocation: 21,
                    SKU: 'SYS-2029BT-HNC0R',
                    DrawerType: 'Twin Family'
                  }
                },
                { label: 'Resume.doc', icon: '', data: 'Resume Document' }]
            },
            {
              label: 'Home',
              data: 'Home Folder',
              expandedIcon: '',
              collapsedIcon: '',
              children: [{ label: 'Invoices.txt', icon: '', data: 'Invoices for this month' }]
            }]
          },
          {
            label: 'Pictures',
            data: 'Pictures Folder',
            expandedIcon: '',
            collapsedIcon: '',
            children: [
              { label: 'barcelona.jpg', icon: '', data: 'Barcelona Photo' },
              { label: 'logo.jpg', icon: '', data: 'PrimeFaces Logo' },
              { label: 'primeui.png', icon: '', data: 'PrimeUI Logo' }]
          },
          {
            label: 'Movies',
            data: 'Movies Folder',
            expandedIcon: 'fa fa-folder-open',
            collapsedIcon: 'fa fa-folder',
            children: [{
              label: 'Al Pacino',
              data: 'Pacino Movies',
              children: [
                { label: 'Scarface', icon: 'fa fa-file-video-o', data: 'Scarface Movie' },
                { label: 'Serpico', icon: 'fa fa-file-video-o', data: 'Serpico Movie' }]
            },
            {
              label: 'Robert De Niro',
              data: 'De Niro Movies',
              children: [
                { label: 'Goodfellas', icon: 'fa fa-file-video-o', data: 'Goodfellas Movie' },
                { label: 'Untouchables', icon: 'fa fa-file-video-o', data: 'Untouchables Movie' }]
            }]
          }
        ]
    };

    return of(structure).pipe(delay(500));
  }
}

interface SystemInfo {
  Manufacturer: string;
  DrawerHeight: number;
  DrawerLocation: number;
  SKU: string;
  DrawerType: string;
}
